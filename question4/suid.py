import os

euid = os.geteuid()
egid = os.getegid()

print(f'EUID = {euid} & EGID = {egid}')