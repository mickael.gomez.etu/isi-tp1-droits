#ifndef PWG_H_
#define PWG_H_
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define PASSWORD_MAX_SIZE 1024
#define BUFFER_MAX_SIZE 1024
#define PATH_TEMP_FILE "/etc/admini/temp"
#define PATH_PWD_FILE "/etc/admini/passwd"

int removeFile(char * f);
int insertNewPaswd(int fisrtPwd, char *username, char *newPwd, FILE *temp);

#endif
