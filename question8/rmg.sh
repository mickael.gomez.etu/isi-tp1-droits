#!/bin/bash

FILE_TO_DELETE="dir_a/file.txt"
PASSWORD="lamda_a_password"

if TRY=$(printf "$PASSWORD\n" | ./rmg "$FILE_TO_DELETE"); then
  echo "Le script fonctionne"
else
  echo "Erreur au lancement du script : $?"
fi