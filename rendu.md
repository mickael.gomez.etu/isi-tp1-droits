# Rendu "Les droits d’accès dans les systèmes UNIX"

## Trinome

- Nom, Prénom, email: FERNANDES, Nicolas, nicolas.fernandes.etu@univ-lille.fr

- Nom, Prénom, email: GOMEZ, Mickael, mickael.gomez.etu@univ-lille.fr

- Nom, Prénom, email: PHU, Valentin, valentin.phu.etu@univ-lille.fr

## Question 1

````
groups toto output -> toto ubuntu
ls -l myFile
````

myFile contient **rw** (read write) pour le groupe *ubuntu* auquel *toto* appartient.
De ce fait le groupe de *toto* peut écrire donc le processus peut être.

## Question 2

**A. Que signigie le *x***

Le **x** signifie **Exécuter**, si on a cette autorisiation alors on peut accéder aux fichiers et aux dossiers du répertoire,
on peut également se déplacer dans la hiérarchie même sans connaitre son contenu.

**B. L'utilisateur *toto* sans les droits d'exécution**

De ce fait après avoir retirer *toto* du groupe *ubuntu*, il n'a plus les droits d'exécution, ce qui l'empêche de naviguer dans le répertoire.

**C. Lister le répertoire avec l'utilisateur *toto***

Le **r** signifie qu'on peut **Lister** son contenu (dossier et fichier).
Les droit d'exécutions permettent de naviguer dans un répertoir et non de le lister, si nous avons les droits de lecture / lister (soit le **r**)
alors nous pouvons continuer de lister un répertoire.


## Question 3

**A. Valeurs des différents ids**

EUID 1001

EGID 1001

RUID 1001

RGID 1001

Le processus n’arrive pas à ouvrir le fichier dans la mesure où ses ids ne lui donnent pas les droits.

**B. Flag set-user-id activé**

EUID 1000

EGID 1000

RUID 1001

RGID 1001

Le processus parvient à ouvrir le fichier car il est exécuté avec les droits du propriétaire grâce au flag.


## Question 4

**A. Script Python et ids**

Les ids affichent EUID = 1000 et EGID = 1000, 
soit les valeurs attendues pour l’utilisateur ubuntu (qui est propriétaire du fichier) même si toto l’a lancé.

**B. Le flag setuid**

Utiliser le setuid flag permettra à tous les utilisateurs d'exécuter des commandes sur le fichier avec les droits du propriétaire (root) sans pour autant avoir eux-mêmes les droits

## Question 5

**A. A quoi sert *chfn***

Chfn permet de modifier les informations d’un utilisateur, full name, login etc..

**B. Lancer la commande *chfn***

````
Ls -al /usr/bin/chfn : -rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn 
````
On peut voir les données à jour : 
````
toto:x:1001:1001:toto,06,0646256943,0323:/home/toto:/bin/bash
````


## Question 6

Ils sont stockés dans */etc/shadow*, pour que n’importe quel utilisateur n'accèdent pas à tous les mot de passe de chacun.
En effet, il faut être **root** pour faire *cat*.

## Question 7

Les scripts sont dans le répertoire **question7**.

Pour mettre en place la structure, il faut créer un troisième groupe (ici *groupe_ab*).

Les commandes pour mettre en place la structure : 

- Ajouter des utilisateurs :

    ````
    adduser lambda_a
    adduser lambda_b
    adduser admin
    ````

- Ajouter des groupes :

    ````
    groupadd groupe_a
    groupadd groupe_b
    groupadd groupe_ab
    ````

- Ajouter les utilisateurs à leur groupe respectif :

    ````
    adduser lambda_a groupe_a
    adduser lambda_b groupe_b
    adduser lambda_a groupe_ab
    adduser lambda_b groupe_ab
    adduser admin groupe_ab
    adduser admin groupe_a
    adduser admin groupe_b
    ````

- Création des répertoirs : 

    ````
    mkdir dir_a
    mkdir dir_b
    mkdir dir_c
    ````

- Ajustement des rôles :

    ````
    chgrp -R groupe_ab dir_c
    chmod -R g+rx,o-rwx dir_c
    chmod g+s dir_a
    chmod g+s dir_b
    ````

On set le uid (setuid) pour que si l’user admin crée un fichier dans dir_a l’user lambda_a puisse le modifier.

- On doit avoir a la fin :

    ````
    drwxrwsr-x 2 ubuntu groupe_a   4096 Jan 20 17:30 dir_a
    drwxrwsr-x 2 ubuntu groupe_b   4096 Jan 20 15:39 dir_b
    drwxr-x--- 2 ubuntu groupe_ab  4096 Jan 20 16:51 dir_c`
    ```



## Question 8

Le programme et les scripts dans le repertoire **question8**.

## Question 9

Le programme et les scripts dans le repertoire **question9**.

## Question 10

Les programmes **groupe_server** et **groupe_client** dans le repertoire **question10** ainsi que les tests. 
