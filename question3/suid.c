#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    printf("EUID=%d, EGID=%d, RUID=%d, RGID=%d \n", geteuid(), getegid(), getuid(), getgid());

    char c[1000];
    FILE *fptr;

    if ((fptr = fopen("./mydir/mydata.txt", "r")) == NULL)
    {
        printf("Error! Can't read file.");
        exit(1);         
    }

    // read the text until newline 
    fscanf(fptr,"%[^\n]", c);

    printf("Data from the file:\n%s\n", c);
    fclose(fptr);

    return 0;
}
