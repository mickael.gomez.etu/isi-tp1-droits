#!/bin/bash

cat dir_b/* && cat dir_c/* && cat dir_a/*

# THINGS WE CAN DO

# A
update_a=$(echo "should work" >> dir_a/a_file.txt)
create_a=$(touch dir_a/new_a_file.txt)
dir_a=$(mkdir dir_a/new_a_dir)
delete_a=$(rm dir_a/a_not_owned.txt)
rename_a=$(mv dir_/a_not_owned.txt dir_a/new_a_not_owned.txt)

# B
update_b=$(echo "should work" >> dir_b/b_file.txt)
create_b=$(touch dir_b/new_b_file.txt)
dir_b=$(mkdir dir_b/new_b_dir)
delete_b=$(rm dir_b/b_not_owned.txt)
rename_b=$(mv dir_/b_not_owned.txt dir_b/new_b_not_owned.txt)

# C
update_c=$(echo "should not work" >> dir_c/c_file.txt)
create_c=$(touch dir_c/new_c_file.txt)
delete_c=$(rm dir_c/c_not_owned.txt)
rename_c=$(mv dir_/c_not_owned.txt dir_c/new_c_not_owned.txt)