#!/bin/bash

cat dir_a/* && cat dir_c/*

# THINGS WE CAN'T DO

# C
update_c=$(echo "should not work" >> dir_c/c_file.txt)
rename_c=$(mv dir_c/c_file.txt dir_c/c_file_update.txt)
delete_c=$(rm dir_c/c_file.txt)
create_c=$(touch dir_c/new_c_file.txt)
# A
delete_a=$(rm dir_a/a_not_owned.txt)
rename_a=$(mv dir_a/a_not_owned.txt dir_a/new_a_not_owned.txt)
# B
read_b=$(cat dir_b/b_file.txt)
update_b=$(echo "should not work" >> dir_b/b_file.txt)
delete_b=$(rm dir_b/b_file.txt)
create_b=$(touch dir_b/new_b_file.txt)

# THINGS WE CAN DO

# A
update_a=$(echo "should work" >> dir_a/a_file.txt)
create_a=$(touch dir_a/new_a_file.txt)
dir_a=$(mkdir dir_a/new_a_dir)