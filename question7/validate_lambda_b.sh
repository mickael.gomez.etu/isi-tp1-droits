#!/bin/bash

cat dir_b/* && cat dir_c/*

# THINGS WE CAN'T DO

# C
update_c=$(echo "should not work" >> dir_c/c_file.txt)
rename_c=$(mv dir_c/c_file.txt dir_c/c_file_update.txt)
delete_c=$(rm dir_c/c_file.txt)
create_c=$(touch dir_c/new_c_file.txt)
# B
delete_b=$(rm dir_b/b_not_owned.txt)
rename_b=$(mv dir_/b_not_owned.txt dir_b/new_b_not_owned.txt)
# A
read_a=$(cat dir_a/a_file.txt)
update_a=$(echo "should not work" >> dir_a/a_file.txt)
delete_a=$(rm dir_a/a_file.txt)
create_a=$(touch dir_a/new_a_file.txt)

# THINGS WE CAN DO

# B
update_b=$(echo "should work" >> dir_b/b_file.txt)
create_b=$(touch dir_b/new_b_file.txt)
dir_b=$(mkdir dir_b/new_b_dir)